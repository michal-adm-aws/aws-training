<img src="https://welastic.pl/wp-content/uploads/2021/10/alex.png" alt="Welastic logo" width="100" align="left">
<br><br>
<br><br>
<br><br>

# Deploying EC2 instances in VPC

## LAB Overview

#### In this lab session, you will create two EC2 instances, one on the public network, the other on the private network. You will use the SSH protocol to connect to the public instance and use it as a jump host to connect to the EC2 machine located in the private network. We will not use your computer, but will use the "CloudShell" functionality. Moreover to acomplish this, we will use the network created earlier.

![](img/EC2_topology.png)

In this lab you will create:

* Create a RSA keys
* Create a EC2 instance in a public subnet
* Create a EC2 instance in a private subnet
* Initialize your CloudShell temporary account
* SSH into a first machine and use it as a jump host for a second SSH connection

## Task 1: Create a RSA keys
You will create a pair of RSA keys, which is necessary (if you want to access the EC2 machine) during the process of creating an EC2 instance (both Windows and Linux)

1. In the **AWS Management Console**, on the **Services** menu, click **EC2**.
2. In the navigation pane on the left, click **Key Pairs** (under *Network & Security* section).
3. Click **Create key pair**.
4. In the **Create key pair** window use the following:
   * **Name:** StudentX_keys (where X is your Student number)
   * **Key pair type:** leave default RSA type
   * **Private key file format:** leave default ".pem" extension
   * Click **Create key pair** and download your private key.

## Task 2: Create a public EC2 instance

In this task, you will create a small size EC2 instance and place it inside a public subnet.

1. In the **AWS Management Console**, on the **Services** menu, click **EC2**.
2. In the navigation pane on the left, click **Instances**.
3. Click **Launch instances**
4. In the **Launch an instance** window configure the following:
   * **Name:** StudentX_Public_EC2
   * **Application and OS Images (Amazon Machine Image):** make sure that latest Amazon Linux is selected (marked by default)
   * **Instance type:** select t3.small
   * **Key pair (login)**: select from the list RSA keys that you have created previously (Task 1)
   * **Network settings:** click on Edit button:
     * **VPC:** select here VPC ID from our previous lab 
     * **Subnet:** please select from the list of available networks, the first public network
     * **Auto-assign public IP:** make sure that it is set to enabled mode
     * **Firewall (security groups):** switch to the "Select existing security group" option
     * **Common security groups:** select created in the previous lab security group for public access - name should be similar to "StudentX_WebServers"
   * **Configure storage:** change a value from 8 GiB to 20 GiB, and leave gp2 as a default drive type
   * **Advanced details:** Expand this option and scroll down to the "User Data" section. Paste there this snippet of code:

   ```
   #!/bin/sh
   yum update -y  
   yum install httpd -y  
   service httpd start
   ```

   * Click **Launch instance**
   * Wait for a confirmation and then click "View all instances"
   * Give the virtual machine a while to start all the processes. Check the public IP address of the EC2 instance you created by selecting it and reading its properties. Try opening a web browser and typing: **http://IP_OF_PUBLIC_EC2**

## Task 3: Create a private EC2 instance

In this task, you will create a small size EC2 instance and place it inside a private subnet. By default, if you place objects in a private network, then you will not be able to access them from the Internet, as well as they themselves will not have access to the world.

1. In the **AWS Management Console**, on the **Services** menu, click **EC2**.
2. In the navigation pane on the left, click **Instances**.
3. Click **Launch instances**
4. In the **Launch an instance** window configure the following:
   * **Name:** StudentX_Private_EC2
   * **Application and OS Images (Amazon Machine Image):** make sure that latest Amazon Linux is selected (marked by default)
   * **Instance type:** select t3.small
   * **Key pair (login)**: select from the list RSA keys that you have created previously (Task 1)
   * **Network settings:** click on Edit button:
     * **VPC:** select here VPC ID from our previous lab 
     * **Subnet:** please select from the list of available networks, the first private network
     * **Auto-assign public IP:** make sure that it is set to disabled mode
     * **Firewall (security groups):** switch to the "Select existing security group" option
     * **Common security groups:** select created in the previous lab security group for private access - name should be similar to "StudentX_DB"
   * **Configure storage:** change a value from 8 GiB to 20 GiB, and leave gp2 as a default drive type
   * Click **Launch instance**
   * Wait for a confirmation and then click "View all instances"

## Task 4: Initialize your CloudShell account and EC2 instances

AWS CloudShell is a browser-based shell that makes it easy to securely manage, explore, and interact with your AWS resources. CloudShell is pre-authenticated with your console credentials. Common development and operations tools are pre-installed, so no local installation or configuration is required. 

1. In the **AWS Management Console**, on the **Services** menu, click **CloudShell** and accept welcome screen.
2. Under **Actions** button, click **Upload file**.
3. In a new windows, please choose your downloaded pem file for SSH key. Click on **Select file** and **Upload**.
4. After a while you should see your pem file inside local environment (CloudShell), check it by running the command: `ls`
5. Set the correct file permissions for SSH keys using the command: `chmod 400 FILE_NAME`
6. Now go back for a moment to EC2 console - open a new window and from **AWS Management Console**, on the **Services** menu, click **EC2**.
7. In the navigation pane on the left, click **Instances**.
8. Select (using checkmark) your public EC2 machine and read its public IP address - "Public IPv4 address" field (copy it to your clipboard)
9. Go back to CloudShell and try to connect using SSH by issuing the command: `ssh -i KEY_FILE_NAME.pem ec2-user@IP_OF_PUBLIC_EC2` (remember to set the correct values). For a question: "Are you sure you want to continue connecting (yes/no)?" - just answer by typing: `yes`.
10. You have now successfully connected from the CloudShell console to the shell of a private Ubuntu instance. We now need to upload the private ssh key again, this time to this machine. There are many methods (such as the popular SCP), but we will simply use a text editor. Start typing a command: `vi key.pem`.
11. You will see a VI text editor window, in the meantime open the contents of the SSH key file on your own computer, in order to copy it to clipboard. Copy the entire contents (from **-----BEGIN RSA PRIVATE KEY-----**, all the way to **-----END RSA PRIVATE KEY-----**)
12. Being next in the middle of the VI editor, press on the `i` button - at the bottom of the screen should appear a message: **INSERT**. Then, by right-clicking, select *paste* and accept the warning by pressing the *paste* button. After pasting the content, hit the **ESC** button (the **INSERT** text should disappear). To save the changes and close the editor, you need to type the character `:` (colon) and then the letters `wq` and press enter.
13. The newly created file with the contents of our key, again must have the correct permissions set, so execute: `chmod 400 key.pem`
14. As in task no 6-8. check the IP address (private) of the EC2 instance that is on the private network and copy it. Now we should look at the field **Private IPv4 addresses**.
15. Let's try to make an SSH connection to the other EC2 instance via the command: `ssh -i key.pem ec2-user@IP_OF_PRIVATE_EC2`.
16. If all went well, you should now be logged into an EC2 machine that has no connection to the world.

## Task 5: Clean EC2 instances

1. In the **AWS Management Console**, on the **Services** menu, click **EC2**.
2. In the navigation pane on the left, click **Instances**.
3. Select (checkmark) all two EC2 instances and under **Instance State** select **Terminate instance**.
4. In the new window, press the button **Terminate**.

## END LAB

### Conclusion

In this lab, we went through the process of creating virtual machines. Based on our network topology, we opened a machine with a public address and another without one. Using a popular mechanism, so called jump host, we finally managed to log into the private instance.

<br><br>

<p align="right">&copy; 2022 Welastic Sp. z o.o.<p>